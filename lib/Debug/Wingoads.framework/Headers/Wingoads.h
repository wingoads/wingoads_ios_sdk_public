//
//  Wingoads.h
//  Wingoads
//
//  Created by Roman Slysh on 12/24/15.
//  Copyright © 2015 Wingoads. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//! Project version number for Wingoads.
FOUNDATION_EXPORT double WingoadsVersionNumber;

//! Project version string for Wingoads.
FOUNDATION_EXPORT const unsigned char WingoadsVersionString[];

@protocol WingoadsDelegate
@optional
- (void)willAppearAd;
- (void)didAppearAd;
- (void)willDissappearAd;
- (void)didDissappearAd;
@end


@interface Wingoads : NSObject

@property (assign) id <WingoadsDelegate> delegate;

- (instancetype)initWithAppId:(NSString*)appId;

- (void)showPopUp;

@end