//
//  ViewController.m
//  WingoadsSample
//
//  Created by Roman Slysh on 1/10/16.
//  Copyright © 2016 Wingoads. All rights reserved.
//

#import "ViewController.h"
#import <Wingoads/Wingoads.h>

@interface ViewController () <WingoadsDelegate>

@property(nonatomic, strong)Wingoads* wingoads;

@end

@implementation ViewController


- (void)viewDidLoad {
  [super viewDidLoad];
  [self showPopUp:nil];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)showPopUp:(id)sender {
  if (self.wingoads == nil) {
    self.wingoads = [[Wingoads alloc] initWithAppId:@"SAMPLE-APP-ID"];
  }
  self.wingoads.delegate = self;
  [self.wingoads showPopUp];
  
}

- (void)willAppearAd {
  NSLog(@"willAppearAd");
}

- (void)didAppearAd {
  NSLog(@"didAppearAd");
}

- (void)willDissappearAd {
  NSLog(@"willDissappearAd");
}

- (void)didDissappearAd {
  NSLog(@"didDissappearAd");
}
@end
