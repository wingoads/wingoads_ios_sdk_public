//
//  main.m
//  WingoadsSample
//
//  Created by Roman Slysh on 1/10/16.
//  Copyright © 2016 Wingoads. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
