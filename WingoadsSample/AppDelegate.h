//
//  AppDelegate.h
//  WingoadsSample
//
//  Created by Roman Slysh on 1/10/16.
//  Copyright © 2016 Wingoads. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

